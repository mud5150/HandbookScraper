require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'restclient'
require 'readline'
require 'uri'

# This is my first ruby script. Go easy on me

def GetLinksInMarkdown(response)
    _doc = Nokogiri::HTML(response)

    _links = _doc.css("div.container.md-page").css('a[href]')

    return _links
end

def IsNotSelfLink(link)
    return ! link.start_with?('#')
end

def HasNotBeenChecked(link)
    return $checkedLinks[link] == nil
end

def ShouldCheckLinks(link)
    return URI(link).relative?
end

def RecusiveLinkCheck(uri)
    begin
        _shouldCheckLinks = ShouldCheckLinks(uri)
    rescue => e
        puts "#{e.message} #{uri}"
    end

    begin
        if _shouldCheckLinks
            _checkUri = $baseUri.merge(uri)
        else
            _checkUri = URI(uri)
        end
        _uriString = _checkUri.to_s
    rescue => e
        puts "#{e.message} #{uri}"
    end


    httpResponseCode = 0
    begin
        _response = RestClient.get(_uriString)
        httpResponseCode = _response.code
    rescue RestClient::ExceptionWithResponse => e
        httpResponseCode = e.http_code
        puts "something else happened: #{_uriString} Response: #{e.http_code}"
    rescue => e
        puts "something else happened: #{_uriString} Response: #{e.message}"
    end

    $checkedLinks[uri] = httpResponseCode

    if _response == nil
        return nil
    end

    _links = GetLinksInMarkdown(_response)

    if _shouldCheckLinks
        puts "From page: #{_uriString}"
        _links.each do |link|
            linkString = link["href"]
            if IsNotSelfLink(linkString) && HasNotBeenChecked(linkString)
                RecusiveLinkCheck(linkString)
            end
        end
    end

    return nil
end

puts "These are not the web pages you're looking for..."

$baseUri = URI('https://about.gitlab.com/')
# _startUri = '/handbook/leadership/career-development-discussion-at-the-1-1' # 404 example
_startUri = '/handbook/'

$checkedLinks = Hash.new

RecusiveLinkCheck(_startUri)